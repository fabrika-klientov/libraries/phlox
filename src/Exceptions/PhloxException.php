<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox\Exceptions;

class PhloxException extends \Exception
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->setMessage($message);
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = 'Phlox::CORE: ' . $message;
    }
}
