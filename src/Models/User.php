<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox\Models;

/**
 * @property string $id
 * @property string $createdTimestamp
 * @property string $username
 * @property string $email
 * @property string $firstName
 * @property string $lastName
 * @property bool $enabled
 * @property bool $totp
 * @property bool $emailVerified
 * @property array $disableableCredentialTypes
 * @property array $requiredActions
 * @property mixed $notBefore
 * @property mixed $access
 * */
class User extends Base
{

}
