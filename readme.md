## Status

[![Latest Stable Version](https://poser.pugx.org/shadoll/lupinus/v/stable)](https://packagist.org/packages/shadoll/lupinus)
[![pipeline status](https://gitlab.com/fabrika-klientov/libraries/lupinus/badges/master/pipeline.svg)](https://gitlab.com/fabrika-klientov/libraries/lupinus/commits/master)
[![License](https://poser.pugx.org/shadoll/lupinus/license)](https://packagist.org/packages/shadoll/lupinus)

---

[doc](https://www.keycloak.org/docs-api/10.0/rest-api/index.html)

[doc](https://github.com/keycloak/keycloak-documentation/blob/master/securing_apps/topics/oidc/oidc-generic.adoc#_token_introspection_endpoint)

[doc](https://www.keycloak.org/docs/latest/securing_apps/#_javascript_adapter)

[doc](https://openid.net/specs/openid-connect-core-1_0.html#AuthorizationEndpoint)

