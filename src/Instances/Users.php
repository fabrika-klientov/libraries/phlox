<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox\Instances;

use Phlox\Models\Social;
use Phlox\Models\User;

class Users extends BaseInstance
{
    protected static $linkGet = 'users';
    protected static $linkCount = 'users/count';

    /**
     * @return mixed|int
     */
    public function count()
    {
        return $this->client->getHttpClient()->get(self::$linkCount);
    }

    /**
     * @param array $filter
     * @return \Illuminate\Support\Collection
     */
    public function get(array $filter = [])
    {
        return collect($this->client->getHttpClient()->get(self::$linkGet, $filter))
            ->map(
                function ($item) {
                    return new User($item);
                }
            );
    }

    /**
     * @param string $id
     * @return User
     */
    public function find(string $id)
    {
        $result = $this->client->getHttpClient()->get(self::$linkGet . "/$id");

        return new User($result);
    }

    /**
     * @param User $user
     */
    public function store(User $user)
    {
        $this->client->getHttpClient()->post(self::$linkGet, $user->jsonSerialize());
    }

    /**
     * @param string $id
     */
    public function delete(string $id)
    {
        $this->client->getHttpClient()->delete(self::$linkGet . "/$id");
    }

    /**
     * @param string $id
     * @return Social|null
     */
    public function social(string $id)
    {
        $result = $this->client->getHttpClient()->get(self::$linkGet . "/$id/federated-identity");

        if (empty($result)) {
            return null;
        }

        return new Social($result);
    }

    /**
     * @param string $id
     */
    public function logout(string $id)
    {
        $this->client->getHttpClient()->get(self::$linkGet . "/$id/logout");
    }

    /**
     * @param string $id
     * @param string $password
     * @param bool $temporary
     */
    public function newPassword(string $id, string $password, bool $temporary = true)
    {
        $this->client->getHttpClient()->put(
            self::$linkGet . "/$id/reset-password",
            [
                'type' => 'password',
                'temporary' => $temporary,
                'value' => $password,
            ]
        );
    }
}
