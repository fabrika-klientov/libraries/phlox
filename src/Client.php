<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox;

use Phlox\Instances;
use Phlox\Services\HttpClient;

class Client
{
    private $username;
    private $password;
    private $oidc;

    /**
     * @param string|null $username
     * @param string|null $password
     * @param array|null $oidc
     */
    public function __construct(string $username = null, string $password = null, array $oidc = null)
    {
        $this->username = $username ?? (function_exists('config') ? config('phlox.admin_username') : null);
        $this->password = $password ?? (function_exists('config') ? config('phlox.admin_password') : null);
        $this->oidc = $oidc ?? (function_exists('config') ? config('phlox.oidc') : null);
    }

    /**
     * @param bool $admin
     * @return HttpClient
     * @throws Exceptions\PhloxException
     */
    public function getHttpClient(bool $admin = true): HttpClient
    {
        return new HttpClient(['username' => $this->username, 'password' => $this->password], $admin, $this->oidc);
    }

    /**
     * @return Instances\Users
     * */
    public function users()
    {
        return new Instances\Users($this);
    }
}
