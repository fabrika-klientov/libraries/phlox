<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox\Models;

/**
 * @property-read string $identityProvider
 * @property-read string $userId
 * @property-read string $userName
 * */
class Social extends Base
{

}
