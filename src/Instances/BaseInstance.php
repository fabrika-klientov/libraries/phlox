<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox\Instances;

use Phlox\Client;

abstract class BaseInstance
{
    protected $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}
