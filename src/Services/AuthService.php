<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox\Services;

class AuthService
{
    private $httpClient;
    private $authData;
    private $authTokens = null;
    private $oidc;
    protected static $authLink = '/auth/realms/{{realm}}/protocol/openid-connect/token';

    /**
     * @param HttpClient $httpClient
     * @param array $authData
     */
    public function __construct(HttpClient $httpClient, array $authData)
    {
        $this->httpClient = $httpClient;
        $this->authData = $authData;
        $this->oidc = $this->httpClient->getOidc();

        self::storeAuthDir();

        if (!empty($authData['username']) && file_exists(static::pathAuth($authData['username'] . '.json'))) {
            $this->authTokens = json_decode(file_get_contents(static::pathAuth($authData['username'] . '.json')), true);
        }
    }

    /**
     * @return array
     * */
    public function getAuthData()
    {
        if (empty($this->authTokens)) {
            return self::auth();
        }

        return $this->authTokens;
    }

    /**
     * @return array
     * */
    public function auth()
    {
        if (empty($this->authTokens)) {
            return self::_auth();
        }

        return self::_refreshAuth();
    }

    /**
     * @return array|null
     */
    private function _auth(): ?array
    {
        return $this->_queryAuth(
            [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $this->oidc['resource'],
                    'username' => $this->authData['username'],
                    'password' => $this->authData['password'],
//                    'scope' => '',
                ]
            ]
        );
    }

    /**
     * @return mixed|null
     */
    private function _refreshAuth()
    {
        $authData = $this->_queryAuth(
            [
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'client_id' => $this->oidc['resource'],
                    'refresh_token' => $this->authTokens['refresh_token'] ?? null,
//                    'scope' => '',
                ]
            ]
        );

        if (empty($authData)) {
            unlink(static::pathAuth($this->authData['username'] . '.json'));

            return self::_auth();
        }

        return $authData;
    }

    /**
     * @param array $options
     * @return mixed|null
     */
    private function _queryAuth(array $options)
    {
        try {
            $result = $this->httpClient->getHttp()
                ->post(
                    str_replace('{{realm}}', $this->oidc['realm'], self::$authLink),
                    array_merge(
                        [
                            'headers' => [
                                'Content-Type' => 'application/x-www-form-urlencoded',
                                'Accept' => 'application/json',
                            ],
                        ],
                        $options
                    )
                );

            $this->authTokens = json_decode($result->getBody()->getContents(), true);

            if (empty($this->authTokens)) {
                return null;
            }

            file_put_contents(static::pathAuth($this->authData['username'] . '.json'), json_encode($this->authTokens));

            return $this->authTokens;
        } catch (\Exception $exception) { // 401 and other
            return null;
        }
    }

    /**
     * @param string|null $file
     * @return string
     */
    protected static function pathAuth(string $file = null): string
    {
        $path = 'phlox/auth/' . ($file ?? '');
        return function_exists('storage_path') ? storage_path($path) : $path;
    }

    /**
     * @return void
     * */
    protected function storeAuthDir()
    {
        if (!file_exists(static::pathAuth())) {
            if (!mkdir(static::pathAuth(), 0775, true)) {
                throw new \RuntimeException('Did\'t stored path for auth data. Permission denied.');
            }
        }
    }
}
