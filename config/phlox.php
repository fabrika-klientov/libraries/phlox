<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

$OIDC_JSON = base_path(env('PHLOX_OIDC_JSON', 'keycloak.json'));

return [
    'oidc' => file_exists($OIDC_JSON) ? json_decode(file_get_contents($OIDC_JSON), true) : null,
    'admin_username' => env('PHLOX_ADMIN_USERNAME'),
    'admin_password' => env('PHLOX_ADMIN_PASSWORD'),
];
