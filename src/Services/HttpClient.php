<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Str;
use Phlox\Exceptions\PhloxException;

class HttpClient
{
    private $client;
    private $authService;
    private $oidc;

    /**
     * @param array $authData
     * @param bool $admin
     * @param array|null $oidc
     * @throws PhloxException
     */
    public function __construct(array $authData, bool $admin = true, array $oidc = null)
    {
        $this->oidc = $oidc ?? (function_exists('config') ? config('phlox.oidc') : null);
        if (empty($this->oidc)) {
            throw new PhloxException('oidc data is required');
        }

        $server = Str::afterLast($this->oidc['auth-server-url'], '/') == 'auth'
            ? ($this->oidc['auth-server-url'] . '/')
            : $this->oidc['auth-server-url'];

        $this->client = new Client(
            [
                'base_uri' => $server . ($admin ? 'admin/' : '') . 'realms/' . $this->oidc['realm'] . '/',
                'verify' => false, // delete
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ],
            ]
        );

        $this->authService = new AuthService($this, $authData);
    }

    /**
     * @param string $link
     * @param array $params
     * @param array $options
     * @return mixed
     */
    public function get(string $link, array $params = [], array $options = [])
    {
        return self::request(
            'GET',
            $link,
            array_merge(
                [
                    'query' => $params,
                ],
                $options
            )
        );
    }

    /**
     * @param string $link
     * @param array $data
     * @param array $options
     * @return mixed
     */
    public function post(string $link, array $data, array $options = [])
    {
        return self::request(
            'POST',
            $link,
            array_merge(
                [
                    'json' => $data,
                ],
                $options
            )
        );
    }

    /**
     * @param string $link
     * @param array $data
     * @param array $options
     * @return mixed
     */
    public function patch(string $link, array $data, array $options = [])
    {
        return self::request(
            'PATCH',
            $link,
            array_merge(
                [
                    'json' => $data,
                ],
                $options
            )
        );
    }

    /**
     * @param string $link
     * @param array $data
     * @param array $options
     * @return mixed
     */
    public function put(string $link, array $data, array $options = [])
    {
        return self::request(
            'PUT',
            $link,
            array_merge(
                [
                    'json' => $data,
                ],
                $options
            )
        );
    }

    /**
     * @param string $link
     * @param array $options
     * @return mixed
     */
    public function delete(string $link, array $options = [])
    {
        return self::request(
            'DELETE',
            $link,
            $options
        );
    }

    /**
     * @return Client
     */
    public function getHttp(): Client
    {
        return $this->client;
    }

    /**
     * @return array
     * */
    public function getOidc()
    {
        return $this->oidc;
    }


    /**
     * @param string $method
     * @param string $link
     * @param array $options
     * @param bool $isStopped
     * @return mixed
     */
    protected function request(string $method, string $link, array $options, bool $isStopped = false)
    {
        try {
            $result = $this->client->request($method, $link, self::authData($options));

            return json_decode($result->getBody()->getContents(), true);
        } catch (ClientException $exception) {
            if ($exception->getCode() == 401 && !$isStopped) {
                $this->authService->auth();

                return $this->request($method, $link, $options, true);
            }

            throw $exception;
        }
    }

    /**
     * @param array $options
     * @return array
     */
    protected function authData(array $options): array
    {
        unset($options['headers']['Authorization']);
        $authData = $this->authService->getAuthData();

        return array_merge_recursive(
            $options,
            ['headers' => ['Authorization' => 'Bearer ' . ($authData['access_token'] ?? null)]]
        );
    }

}
