<?php
/**
 * Created by IntelliJ IDEA.
 * User: jarvis
 * @package   Phlox
 * @author    Andrew <3oosor@gmail.com>
 * @copyright 2020 Fabrika-Klientov
 * @version   GIT: 20.10.06
 * @link      https://fabrika-klientov.ua
 */

namespace Phlox\Models;

abstract class Base implements \JsonSerializable
{
    protected $data;

    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __toString()
    {
        return json_encode($this->data);
    }

    public function jsonSerialize()
    {
        return $this->data;
    }
}
